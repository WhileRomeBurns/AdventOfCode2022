#pragma once

#include <vector>
#include <span>
#include <string>
//#include <string_view>
#include <algorithm>
//#include <numeric>
//#include <limits>
#include <utility>
#include <variant>

#include "Utilities.h"

#define DAY day13

namespace DAY {
	const auto day = STRINGIFY(DAY);

	using StrPair = std::pair<std::string, std::string>;
	using CharInt = std::variant<char, int>;

	/*
	//class Empty {};
	class List;
	using ItemVar = std::variant<int, std::unique_ptr<List>>;
	struct List {
		std::vector<ItemVar> items;
	};
	//using ListVec = std::vector<List>;


	List parseList(const std::string& line) {
		auto list = List{};
		auto depth = 0;
		auto& current = list;
		for (auto i = 1; i < line.size(); i++) {
			if (i == '[') {
				depth++;
				current.items.emplace_back(std::make_unique<ItemVar>());
			}
			else if (i == ']') {
				depth--;
			}



			if (depth < 0)
				break;
		}
		return List{};
	}
	*/

	void getNext() {

	}

	void isSorted(std::string& left, std::string& right) {
		auto sorted = true;
		auto depthLt = 0;
		auto depthRt = 0;
		//auto maxSize = std::max(left.size(), right.size());
		auto il = 0;
		for (auto ir = 0; ir < right.size(); ir++) {
			il = ir;
			if (il > left.size()) {
				sorted = false;
				break;
			}
			auto rt = right[ir];
			auto lt = left[il];

			if (rt == '[')
				depthRt++;
			else if (rt == ']')
				depthRt--;
			if (lt == '[')
				depthLt++;
			else if (lt == ']')
				depthLt--;


		}

	}

	std::vector<std::string> parse(const std::string& line) {
		std::vector<std::string> ret;

		for (const auto& elem : splitStrings(line, ",")) {
			int idx = -1;
			for (char c : elem) {
				if (c == '[' || c == ']') {
					//ret.emplace_back(std::string(1, c));
					ret.emplace_back(1, c);
				}
				else {
					if (idx == -1) {
						ret.emplace_back(1, c);
						idx = ret.size() - 1;
					}
					else {
						// second digit
						ret[idx].push_back(c);
					}

				}
			}
		}

		return ret;
	}

	void printv(auto v) {
		for (auto& i : v)
			fprint("{} ", i);
		fprint("\n");
	}

	void compute(std::span<StrPair> pairs) {
		// iterate each light to the next item. nested track depth.
		//for (const auto& [left, right] : pairs) {
		//	auto a = parseList(left);
		//	auto b = parseList(right);
		//}

		for (const auto& [left, right] : pairs) {
			auto a = parse(left);
			auto b = parse(right);

			printv(a);
			printv(b);
			fprint("\n");
		}

		//for ( auto& [left, right] : pairs) {
		//	isSorted(left, right);
		//}

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("  Part 2");
	}

	void run() {
		auto data = readInputTxt(day, 2);

		std::vector<StrPair> pairs;
		for (auto i = 0; i < data.size() - 1; i += 3)
			pairs.emplace_back(data[i], data[i + 1]);

		compute(pairs);
	}
} // namespace

#undef DAY