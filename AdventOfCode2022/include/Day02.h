#pragma once

#include <string>
#include <vector>
#include <span>

#include "Utilities.h"

#define DAY day02

namespace DAY {
	const auto day = STRINGIFY(DAY);

	void compute(std::span<std::string> data) {
		auto score = 0;
		auto score2 = 0;

		for (const auto& d : data) {
			if (d.empty())
				continue;

			// part1
			if ((d[0] == 'A' && d[2] == 'Z') || (d[0] == 'B' && d[2] == 'X') || (d[0] == 'C' && d[2] == 'Y')) {
				// loss
			}
			else if ((d[0] == 'A' && d[2] == 'X') || (d[0] == 'B' && d[2] == 'Y') || (d[0] == 'C' && d[2] == 'Z')) {
				score += 3; // draw
			}
			else {
				score += 6; // win
			}

			if (d[2] == 'X')
				score += 1; // rock
			else if (d[2] == 'Y')
				score += 2; // paper
			else if (d[2] == 'Z')
				score += 3; //sissors

			// part 2
			if (d[2] == 'X') {
				// need to lose
				if (d[0] == 'A')
					score2 += 3;
				if (d[0] == 'B')
					score2 += 1;
				if (d[0] == 'C')
					score2 += 2;
			}
			else if (d[2] == 'Y') {
				// need to draw
				if (d[0] == 'A')
					score2 += 3 + 1;
				if (d[0] == 'B')
					score2 += 3 + 2;
				if (d[0] == 'C')
					score2 += 3 + 3;
			}
			else if (d[2] == 'Z') {
				// need to win
				if (d[0] == 'A')
					score2 += 6 + 2;
				if (d[0] == 'B')
					score2 += 6 + 3;
				if (d[0] == 'C')
					score2 += 6 + 1;
			}
		}

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tScore: {}", score);
		fprintn("  Part 2");
		fprintn("\tScore: {}", score2);
	}

	void run() {
		auto data = readInputTxt(day);
		compute(data);
	}
} // namespace

#undef DAY