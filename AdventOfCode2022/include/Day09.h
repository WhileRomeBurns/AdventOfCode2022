#pragma once

#include <vector>
#include <set>
#include <span>
#include <utility>

#include "Utilities.h"

#define DAY day09

namespace DAY {
	const auto day = STRINGIFY(DAY);

	struct Pos {
		int x{ 0 };
		int y{ 0 };
	};

	void move(Pos& p, char dir) {
		switch (dir) {
		case 'U':
			p.y += 1; break;
		case 'D':
			p.y -= 1; break;
		case 'L':
			p.x -= 1; break;
		case 'R':
			p.x += 1; break;
		}
	}

	void follow(const Pos& head, Pos& tail) {
		// same col
		if (head.x == tail.x) {
			if (abs(head.y - tail.y) > 1)
				tail.y += head.y > tail.y ? 1 : -1;
		}
		// same row
		else if (head.y == tail.y) {
			if (abs(head.x - tail.x) > 1)
				tail.x += head.x > tail.x ? 1 : -1;
		}
		// diagonol
		else {
			// head =  3,2 | tail = 1,1
			if (abs(head.x - tail.x) + abs(head.y - tail.y) > 2) {
				tail.x += head.x > tail.x ? 1 : -1;
				tail.y += head.y > tail.y ? 1 : -1;
			}
		}
	}

	int simulate(std::span<std::pair<char, int>> steps, int knots) {
		std::vector<Pos> rope(knots);

		std::set<std::pair<int, int>> visited;
		visited.insert(std::make_pair(rope.back().x, rope.back().y));

		for (const auto& s : steps) {
			for (auto i = 0; i < s.second; i++) {
				move(rope[0], s.first);

				for (auto j = 1; j < rope.size(); j++)
					follow(rope[j - 1], rope[j]);
				visited.insert(std::make_pair(rope.back().x, rope.back().y));
			}
		}

		return int(visited.size());
	}

	void compute(std::span<std::pair<char, int>> steps) {
		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tVisited: {}", simulate(steps, 2));

		fprintn("  Part 2", day);
		fprintn("\tVisited: {}", simulate(steps, 10));
	}

	void run() {
		auto data = readInputTxt(day);

		std::vector<std::pair<char, int>> steps;
		for (const auto& line : data) {
			const auto [left, right] = splitPair(line);
			steps.push_back(std::make_pair(left[0], strTo<int>(right)));
		}

		compute(steps);
	}
} // namespace

#undef DAY