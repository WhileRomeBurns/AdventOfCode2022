#pragma once

#include <vector>
#include <span>
#include <string>
//#include <string_view>
//#include <algorithm>
//#include <numeric>
//#include <limits>
//#include <utility>

#include "Utilities.h"

#define DAY day17

namespace DAY {
	const auto day = STRINGIFY(DAY);

	void compute(std::span<std::string> data) {
		fprintn("\n[{}]\n  Part 1", day);
		fprintn("  Part 2");
	}

	void run() {
		auto data = readInputTxt(day);
		compute(data);
	}
} // namespace

#undef DAY