#pragma once

#include <string>
#include <vector>
#include <span>

#include "Utilities.h"

#define DAY day04

namespace DAY {
	const auto day = STRINGIFY(DAY);

	void compute(std::span<std::string> data) {

		auto fullOverlaps = 0;
		auto partOverlaps = 0;

		for (const auto& line : data) {
			auto [left, right] = splitPairSv(line, ",");
			auto [ltStart, ltEnd] = splitPairInt(left, "-");
			auto [rtStart, rtEnd] = splitPairInt(right, "-");

			if ((ltStart >= rtStart && ltEnd <= rtEnd) ||
				(ltStart <= rtStart && ltEnd >= rtEnd))
				fullOverlaps++;

			if ((ltStart >= rtStart && ltStart <= rtEnd) ||
				(ltEnd >= rtStart && ltEnd <= rtEnd) ||
				(rtStart >= ltStart && rtStart <= ltEnd) ||
				(rtEnd >= ltStart && rtEnd <= ltEnd))
				partOverlaps++;
		}

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tFull Overlaps: {}", fullOverlaps);
		fprintn("  Part 2");
		fprintn("\tPart Overlaps: {}", partOverlaps);
	}

	void run() {
		auto data = readInputTxt(day);
		compute(data);
	}
} // namespace

#undef DAY