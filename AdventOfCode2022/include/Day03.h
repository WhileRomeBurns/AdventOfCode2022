#pragma once

#include <string>
#include <string_view>
#include <vector>
#include <span>
#include <map>

#include "Utilities.h"

#define DAY day03

namespace DAY {
	const auto day = STRINGIFY(DAY);

	std::map<char, int> itemCount(const std::string_view side) {
		std::map<char, int> count;

		for (const auto item : side) {
			if (count.contains(item))
				count.at(item) += 1;
			else
				count[item] = 1;
		}
		return count;
	}

	char misplaced(const std::string_view left, const std::string_view right) {
		for (const auto& [item, count] : itemCount(left))
			if (itemCount(right).contains(item))
				return item;
		return '-';
	}

	char badge(const std::string_view a, const std::string_view b, const std::string_view c) {
		for (const auto& [item, count] : itemCount(a))
			if (itemCount(b).contains(item) && itemCount(c).contains(item))
				return item;
		return '-';
	}

	int score(char c) {
		// scores are 1 - 26 for lowercase and 27 - 52 uppercase
		// in ascii, uppercase values are 65-90 and lowercase 97 - 122
		auto score = int(c);
		if (score >= 65 && score <= 90)
			return score - 38;
		if (score >= 97 && score <= 122)
			return score - 96;
		return 0;
	}

	void compute(std::span<std::string> data) {
		auto count = 0;
		for (const auto& line : data) {
			if (line.empty())
				continue;

			auto left = line.substr(0, line.size() / 2);
			auto right = line.substr(line.size() / 2, line.size() / 2);

			auto m = misplaced(left, right);
			count += score(m);
		}

		auto badgeCount = 0;
		for (std::size_t i = 0; i < data.size(); i += 3) {
			auto m = badge(data[i], data[i + 1], data[i + 2]);
			badgeCount += score(m);
		}

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tScore: {}", count);
		fprintn("  Part 2");
		fprintn("\tScore: {}", badgeCount);
	}

	void run() {
		auto data = readInputTxt(day);
		compute(data);
	}
} // namespace

#undef DAY