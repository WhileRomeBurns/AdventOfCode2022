#pragma once

#include <vector>
#include <span>
#include <string>
#include <cstdint>

#include "Utilities.h"

#define DAY day11

namespace DAY {
	const auto day = STRINGIFY(DAY);

	enum class Operation { add, mult, sq };

	struct Monkey {
		std::vector<int64_t> items;
		int64_t inspected{ 0 };
		Operation op;
		int64_t opValue;
		int64_t test;
		int trueMonkey;
		int falseMonkey;
	};

	void applyOp(int64_t& item, Operation op, int64_t opValue) {
		if (op == Operation::add)
			item += opValue;
		else if (op == Operation::mult)
			item *= opValue;
		else
			item *= item;
	}

	bool divisible(int64_t a, int64_t b) {
		return a % b == 0 ? true : false;
	}

	void simulateRound(std::span<Monkey> monkey, bool part1, int64_t lcm = 1) {
		for (auto& m : monkey) {
			for (auto& currentItem : m.items) {
				if (part1) {
					applyOp(currentItem, m.op, m.opValue);
					currentItem /= 3; // reduce worry and round down
				}
				else {
					applyOp(currentItem, m.op, m.opValue);
					currentItem %= lcm; // modulo to preserve the test divisions
				}

				int dst = divisible(currentItem, m.test) ? m.trueMonkey : m.falseMonkey;

				monkey[dst].items.push_back(currentItem);

				m.inspected++;
			}
			// done inspecting, all items have been tossed to other monkeys
			m.items.clear();
		}
	}

	void printInspections(const std::vector<Monkey>& monkey, int round) {
		fprintn("\n[Round {}]", round);
		for (int i = 0; auto & m : monkey) {
			fprintn("Monkey {} inspected items {}", i, m.inspected);
			i++;
		}
	}

	void compute1(std::vector<Monkey> monkey, int rounds) { // copy
		for (auto r = 0; r < rounds; r++)
			simulateRound(monkey, true);

		sort(monkey.begin(), monkey.end(),
			[](const Monkey& a, const Monkey& b) -> bool { return a.inspected > b.inspected; }
		);

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tMonkey Business: {}", monkey[0].inspected * monkey[1].inspected);
	}

	void compute2(std::vector<Monkey> monkey, int rounds) { // copy
		int64_t lcm = 1;
		for (const auto& m : monkey)
			lcm *= m.test;

		for (auto r = 0; r < rounds; r++) {
			simulateRound(monkey, false, lcm);

			// if (r + 1 == 1 || r + 1 == 20 || (r + 1) % 1000 == 0)
			//	printInspections(monkey, r + 1);
		}

		sort(monkey.begin(), monkey.end(),
			[](const Monkey& a, const Monkey& b) -> bool { return a.inspected > b.inspected; }
		);

		fprintn("  Part 2");
		fprintn("\tMonkey Business: {}", monkey[0].inspected * monkey[1].inspected);
	}

	void run() {
		auto data = readInputTxt(day);

		std::vector<Monkey> monkey;

		for (int i = 0; const auto & line : data) {
			if (line.starts_with("Monkey")) {
				monkey.push_back(Monkey{});
				continue;
			}

			auto words = splitStrings(line);

			if (line.starts_with("  Starting")) {
				const auto end = line.substr(18, line.size() - 18);
				for (auto item : splitNumbers(end, ", "))
					monkey[i].items.push_back(item);
			}
			else if (line.starts_with("  Operation")) {
				if (line.contains("+")) {
					monkey[i].op = Operation::add;
					monkey[i].opValue = strTo<int>(words.back());
				}
				else {
					if (line.contains("old * old")) {
						monkey[i].op = Operation::sq;
						monkey[i].opValue = 1;
					}
					else {
						monkey[i].op = Operation::mult;
						monkey[i].opValue = strTo<int>(words.back());
					}
				}
			}
			else if (line.starts_with("  Test")) {
				monkey[i].test = strTo<int>(words.back());
			}
			else if (line.starts_with("    If true")) {
				monkey[i].trueMonkey = strTo<int>(words.back());
			}
			else if (line.starts_with("    If false")) {
				monkey[i].falseMonkey = strTo<int>(words.back());
				i++;
			}
		}

		compute1(monkey, 20);
		compute2(monkey, 10000);
	}
} // namespace

#undef DAY