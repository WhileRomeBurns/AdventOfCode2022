#pragma once

#include <string_view>
#include <deque>
#include <algorithm>

#include "Utilities.h"

#define DAY day06

namespace DAY {
	const auto day = STRINGIFY(DAY);

	bool unique(std::deque<char> msg) { // copy
		std::sort(msg.begin(), msg.end());
		for (auto i = 0; i < msg.size() - 1; i++)
			if (msg[i] == msg[i + 1])
				return false;
		return true;
	}

	int scan(const std::string_view data, int bufferSize) {
		std::deque<char> msg;
		for (auto i = 0; i < bufferSize; i++)
			msg.push_back(data[i]);

		for (auto i = bufferSize; i < data.size(); i++) {
			if (unique(msg))
				return i;
			msg.pop_front();
			msg.push_back(data[i]);
		}
		return 0;
	}

	void compute(const std::string_view data) {
		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tStart: {}", scan(data, 4));
		fprintn("  Part 2", day);
		fprintn("\tStart: {}", scan(data, 14));
	}

	void run() {
		const auto data = readInputTxt(day)[0];
		compute(data);
	}
} // namespace

#undef DAY