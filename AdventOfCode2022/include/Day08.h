#pragma once

#include <string>
#include <vector>
#include <span>

#include "Utilities.h"

#define DAY day08

namespace DAY {
	const auto day = STRINGIFY(DAY);

	void compute1(std::span<std::vector<int>> grid, std::span<std::vector<int>> visGrid) {
		auto ymax = int(grid.size());
		auto xmax = int(grid[0].size());

		for (auto y = 0; y < ymax; y++) {
			auto tallest = -1;
			for (auto x = 0; x < xmax; x++) { // left -> right
				if (grid[y][x] > tallest) {
					visGrid[y][x] = 1;
					tallest = grid[y][x];
				}
			}
			tallest = -1;
			for (auto x = xmax - 1; x >= 0; x--) { // right -> left
				if (grid[y][x] > tallest) {
					visGrid[y][x] = 1;
					tallest = grid[y][x];
				}
			}
		}

		for (auto x = 0; x < xmax; x++) {
			auto tallest = -1;
			for (auto y = 0; y < ymax; y++) { // top -> bottom
				if (grid[y][x] > tallest) {
					visGrid[y][x] = 1;
					tallest = grid[y][x];
				}
			}
			tallest = -1;
			for (auto y = ymax - 1; y >= 0; y--) { // bottom -> top
				if (grid[y][x] > tallest) {
					visGrid[y][x] = 1;
					tallest = grid[y][x];
				}
			}
		}

		int totalVisible = 0;
		for (const auto& row : visGrid)
			for (auto r : row)
				totalVisible += r;

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tVisible:   {}", totalVisible);
	}

	int score(std::span<std::vector<int>> grid, int x, int y, int xmax, int ymax) {
		int score = 1;
		auto treeHeight = grid[y][x];

		if (x == 0 || y == 0 || x == xmax - 1 || y == ymax - 1)
			return 0;

		// horizontal scan right and left
		auto seen = 0;
		for (auto j = x + 1; j < xmax; j++) {
			seen++;
			if (grid[y][j] >= treeHeight)
				break;
		}
		score *= seen;

		seen = 0;
		for (auto j = x - 1; j >= 0; j--) {
			seen++;
			if (grid[y][j] >= treeHeight)
				break;
		}
		score *= seen;

		// vert scan
		seen = 0;
		for (auto k = y + 1; k < ymax; k++) {
			seen++;
			if (grid[k][x] >= treeHeight)
				break;
		}
		score *= seen;

		seen = 0;
		for (auto k = y - 1; k >= 0; k--) {
			seen++;
			if (grid[k][x] >= treeHeight)
				break;
		}
		score *= seen;

		return score;
	}

	void compute2(std::span<std::vector<int>> grid) {
		auto ymax = int(grid.size());
		auto xmax = int(grid[0].size());

		int bestScore = 0;
		for (auto y = 0; y < ymax; y++) {
			for (auto x = 0; x < xmax; x++) {

				int s = score(grid, x, y, xmax, ymax);
				if (s > bestScore)
					bestScore = s;
			}
		}

		fprintn("  Part 2");
		fprintn("\tBest Score: {}", bestScore);
	}

	void run() {
		auto data = readInputTxt(day);

		std::vector<std::vector<int>> grid;
		std::vector<std::vector<int>> visGrid;
		for (const auto& line : data) {
			grid.push_back(splitDigits(line));
			visGrid.push_back(std::vector<int>(grid[0].size(), 0));
		}

		compute1(grid, visGrid);
		compute2(grid);
	}
} // namespace

#undef DAY