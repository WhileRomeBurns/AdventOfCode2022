#pragma once

#include <string>
#include <map>
#include <span>

#include "Utilities.h"

#define DAY day07

namespace DAY {
	const auto day = STRINGIFY(DAY);

	struct Entry {
		int size{ 0 };
		bool dir{ true };
	};

	std::map<std::vector<std::string>, Entry> parseFileSystem(std::span<std::string> data) {
		// first pass we store everything in a map, second past we update directory sizes
		std::map<std::vector<std::string>, Entry> fs;
		std::vector<std::string> currentPath;

		for (const auto& line : data) {

			if (line.starts_with("$ cd")) {
				auto dir = line.substr(5, line.size() - 5);

				if (dir == "..") {
					currentPath.pop_back();
				}
				else {
					currentPath.push_back(dir);
					if (!fs.contains(currentPath))
						fs[currentPath] = Entry{ .size = 0, .dir = true };
				}
			}
			else if (line.starts_with("$ ls")) {
				/* no need to do anything, we'll grab files below */
			}
			else {
				auto [sizeOrDir, name] = splitPair(line);

				if (std::isdigit(sizeOrDir.data()[0])) {
					auto size = strTo<int>(sizeOrDir);

					currentPath.push_back(name);
					if (!fs.contains(currentPath))
						fs[currentPath] = Entry{ .size = size, .dir = false };
					currentPath.pop_back();
				}
			}
		}

		return fs;
	}

	void updateDirSizes(std::map<std::vector<std::string>, Entry>& fs) {
		// add each file's size to the directory above
		for (auto& [path, entry] : fs) {
			if (!entry.dir) {
				auto copy{ path };

				for (auto i = 0; i < path.size() - 1; i++) {
					copy.pop_back();

					if (fs.contains(copy))
						fs[copy].size += entry.size;
					else
						fprintn("*** Error");
				}
			}
		}
	}

	void compute(std::span<std::string> data) {
		auto fs = parseFileSystem(data);
		updateDirSizes(fs);

		// Part 1
		auto sumOfSmall = 0;
		for (auto& [path, entry] : fs)
			if (entry.dir && entry.size <= 100000)
				sumOfSmall += entry.size;

		// Part 2
		std::vector<std::string> root{ "/" };
		auto total = 70000000;
		auto update = 30000000;
		auto free = update - (total - fs[root].size);
		auto smallestSize = total;

		for (auto& [path, entry] : fs)
			if (entry.dir && entry.size >= free && entry.size < smallestSize)
				smallestSize = entry.size;

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tSum of dirs: {}", sumOfSmall);
		fprintn("  Part 2");
		fprintn("\tSize of dir: {}", smallestSize);
	}

	void run() {
		auto data = readInputTxt(day);
		compute(data);
	}
} // namespace

#undef DAY