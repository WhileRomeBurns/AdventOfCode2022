#pragma once

#include <vector>
#include <span>
#include <string>
#include <deque>
#include <limits>

#include "AdventTypes.h"
#include "Utilities.h"

#define DAY day12

namespace DAY {
	const auto day = STRINGIFY(DAY);

	using Grid = std::vector<std::vector<int>>;
	using GridSpan = std::span<std::vector<int>>;

	int get(GridSpan grid, const Pos& p) { return grid[p.y][p.x]; }
	void set(GridSpan grid, const Pos& p, int v) { grid[p.y][p.x] = v; }

	std::vector<Pos> neighbors(GridSpan height, const Pos& pos) {
		std::vector<Pos> nbrs;

		if (pos.x + 1 < height[0].size())
			nbrs.push_back(Pos{ pos.x + 1, pos.y });
		if (pos.y + 1 < height.size())
			nbrs.push_back(Pos{ pos.x, pos.y + 1 });
		if (pos.x - 1 >= 0)
			nbrs.push_back(Pos{ pos.x - 1, pos.y });
		if (pos.y - 1 >= 0)
			nbrs.push_back(Pos{ pos.x, pos.y - 1 });

		return nbrs;
	}

	void pushBackIfUnique(std::deque<Pos>& que, const Pos& pos) {
		for (const auto& q : que)
			if (q.x == pos.x && q.y == pos.y)
				return;

		que.push_back(pos);
	}

	int pathCost(GridSpan height, Pos start, Pos end) {
		Grid cost;
		Grid visited;
		for (const auto& h : height) {
			cost.push_back(std::vector<int>(h.size(), std::numeric_limits<int>::max()));
			visited.push_back(std::vector<int>(h.size(), 0));
		}
		set(cost, start, 0);

		std::deque<Pos> que;
		que.push_back(start);

		while (!que.empty()) {
			auto current = que.front();
			que.pop_front();
			set(visited, current, 1);

			for (const auto& n : neighbors(height, current)) {

				if (get(height, n) - get(height, current) > 1)
					continue;

				auto currentCost = get(cost, current);

				if (get(height, n) - get(height, current) <= 1) {
					if (currentCost + 1 < get(cost, n)) {
						set(cost, n, currentCost + 1);
					}
				}

				if (get(visited, n) == 0)
					pushBackIfUnique(que, n);
			}
		}

		return get(cost, end);
	}

	void compute(GridSpan height, Pos start, Pos end) {

		auto solution1 = pathCost(height, start, end);

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tCost: {}", solution1);

		auto solution2 = std::numeric_limits<int>::max();

		for (auto y = 0; y < height.size(); y++) {
			for (auto x = 0; x < height[0].size(); x++) {

				if (get(height, Pos{ x,y }) == 1) {
					auto cost = pathCost(height, Pos{ x,y }, end);
					if (cost < solution2)
						solution2 = cost;
				}
			}
		}

		fprintn("  Part 2");
		fprintn("\tCost: {}", solution2);
	}

	void run() {
		Grid height;
		Pos start, end;

		for (auto y = 0; auto & line : readInputTxt(day)) {
			if (auto x = line.find('S'); x != std::string::npos) {
				start = { int(x), y };
				line[x] = 'a';
			}
			if (auto x = line.find('E'); x != std::string::npos) {
				end = { int(x), y };
				line[x] = 'z';
			}

			std::vector<int> row;
			for (char c : line)
				row.push_back(int(c - 96));
			height.push_back(row);

			y++;
		}

		compute(height, start, end);
	}
} // namespace

#undef DAY