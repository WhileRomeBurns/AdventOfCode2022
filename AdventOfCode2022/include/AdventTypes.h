#pragma once

struct Pos {
	// Simple 2d position struct, see posComparator() below.
	int x{ 0 };
	int y{ 0 };
};

auto posComparator() {
	// Useful for map/set. Example:
	//	std::set<Pos, decltype(posComparator())> posSet;

	auto comp = [](const Pos& a, const Pos& b) {
		if (a.x < b.x)
			return true;
		else if (a.x == b.x && a.y < b.y)
			return true;

		return false;
	};

	return comp;
}