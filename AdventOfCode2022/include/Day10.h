#pragma once

#include <string>
#include <vector>
#include <set>
#include <span>
#include <utility>

#include "Utilities.h"

#define DAY day10

namespace DAY {
	const auto day = STRINGIFY(DAY);

	enum class Code { noop, addx };
	std::set<int> signalChecks = { 20, 60, 100, 140, 180, 220 };

	void compute(std::span<std::pair<Code, int>> stack, std::span<std::string> crt) {
		int cycle = 0;
		int pixel = 0;
		int X = 1;
		int Y = 0;
		int signalSum = 0;

		for (const auto& inst : stack) {
			int wait = inst.first == Code::noop ? 1 : 2;

			for (auto i = 0; i < wait; i++) {
				if (abs(X - pixel) < 2)
					crt[Y][pixel] = '#';

				cycle++;
				pixel = (pixel + 1) % 40;

				if (signalChecks.contains(cycle))
					signalSum += cycle * X;
				else if (signalChecks.contains(cycle + 20))
					Y++;
			}

			if (inst.first == Code::addx)
				X += inst.second;
		}

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tSignal Sum: {}", signalSum);
		fprintn("  Part 2");
		printVec(crt, "\n", false);
	}

	void run() {
		auto data = readInputTxt(day);
		std::vector<std::pair<Code, int>> stack;

		for (const auto& line : data) {
			if (line.starts_with("noop")) {
				stack.emplace_back(Code::noop, 0);
			}
			else if (line.starts_with("addx")) {
				const auto& [left, right] = splitPair(line);
				stack.emplace_back(Code::addx, strTo<int>(right));
			}
		}

		// crt screen for part 2, 40x6 text display
		std::vector<std::string> crt;
		for (auto i = 0; i < 6; i++)
			crt.push_back(std::string("........................................"));

		compute(stack, crt);
	}
} // namespace

#undef DAY