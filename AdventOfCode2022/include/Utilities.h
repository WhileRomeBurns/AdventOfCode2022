#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>
#include <vector>
#include <span>
#include <string>
#include <string_view>
#include <format>
#include <ranges>
#include <algorithm>

#define STRINGIFY(s) STRINGIFY_INNER(s)
#define STRINGIFY_INNER(s) #s

std::vector<std::string> readInputTxt(std::string_view day, int offset = 0) {
	// Reads the input.txt as lines from the day: "./data/day##/input.txt"
	// Optionally access a test input#.txt with offset.
	std::string filePath;
	if (offset == 0)
		filePath = std::format("./data/{}/input.txt", day);
	else
		filePath = std::format("./data/{}/input{}.txt", day, offset);
	std::vector<std::string> data;

	std::ifstream input{ filePath };
	if (input.is_open())
	{
		std::string line;
		while (std::getline(input, line))
			data.push_back(line);
		input.close();
	}

	return data;
}

template <typename T>
struct std::formatter<std::vector<T>> {
	constexpr auto parse(std::format_parse_context& ctx) -> decltype(ctx.begin()) {

		for (auto it = ctx.begin(); it != ctx.end(); ++it) {
			if (*it == '}')
				return it;
		}

		return ctx.end();
	}

	template <typename FormatContext>
	auto format(const std::vector<T>& vec, FormatContext& ctx) -> decltype(ctx.out()) {

		auto&& out = ctx.out();

		if (vec.size() > 0)
			std::format_to(out, "{}", vec[0]);

		for (auto i = 1; i < vec.size(); i++)
			std::format_to(out, " {}", vec[i]);

		return out;
	}
};

template <typename... Args>
void fprint(std::string_view fmt, Args&&... args) {
	// std::cout << std::format(fmt, std::forward<Args>(args)...);
	// std::format has a compile time format requirement now, but we can use vformat()
	// https://developercommunity.visualstudio.com/t/std::format-:-error-C7595/1694324
	std::cout << std::vformat(fmt, std::make_format_args(std::forward<Args>(args)...));
}

template <typename... Args>
void fprintn(std::string_view fmt, Args&&... args) {
	// std::cout << std::format(fmt.data() + std::string("\n"), std::forward<Args>(args)...);
	std::cout << std::vformat(fmt.data() + std::string("\n"), std::make_format_args(std::forward<Args>(args)...));
}

template <typename T>
std::string vecToStr(std::span<T> vec, std::string_view sep = " ", bool endNewline = true) {
	std::stringstream ss;
	for (auto& v : vec)
		ss << v << sep;

	if (endNewline)
		ss << std::endl;

	return ss.str();
}

template <typename T>
void printVec(std::span<T> vec, std::string_view sep = " ", bool endNewline = true) {
	std::cout << vecToStr<T>(vec, sep, endNewline);
}

template <typename T>
T strTo(std::string_view sv) {
	// modern way to do stoi, stol, stoll
	T result{};
	std::from_chars(sv.data(), sv.data() + sv.size(), result);
	return result;
}

std::vector<int> biFill(int start, int end) {
	// Bidirectionally returns a vector with values from start to end, either direction
	// Example:
	//     biFill(2, -3)   ->   { 2, 1, 0, -1, -2, -3 }

	int n = std::abs(start - end) + 1;
	int step = start < end ? 1 : -1;

	std::vector<int> result;
	result.resize(n);

	std::ranges::generate_n(result.begin(), n, [step, &start]() { int x = start; start += step; return x; });

	return result;
}

std::pair<std::string_view, std::string_view> splitPairSv(std::string_view str, std::string_view delimiter = " ")
{
	const auto pos = str.find(delimiter);

	std::string_view a = str.substr(0, pos);
	std::string_view b = str.substr(pos + delimiter.length());

	return { a, b };
}

std::pair<std::string, std::string> splitPair(std::string_view str, std::string_view delimiter = " ")
{
	const auto [a, b] = splitPairSv(str, delimiter);
	return { std::string{a.begin(), a.end()}, std::string{b.begin(), b.end()} };
}

std::pair<int, int> splitPairInt(std::string_view str, std::string_view delimiter = " ")
{
	const auto [a, b] = splitPairSv(str, delimiter);
	return { std::stoi(std::string{a.begin(), a.end()}), std::stoi(std::string{b.begin(), b.end()}) };
}

std::vector<int> splitNumbers(std::string_view str, std::string_view delimiter = " ")
{
	std::size_t pos{ 0 };
	std::size_t offset{ 0 };
	std::vector<int> result;

	while (offset < str.length()) {

		pos = str.find(delimiter, offset);

		if (pos != std::string_view::npos) {

			const std::string_view a = str.substr(offset, pos - offset);

			if (a != delimiter && a.size() > 0)
				result.push_back(std::stoi(std::string(a.begin(), a.end())));

			offset = pos + delimiter.length();
		}
		else {
			// check if we have one more value after the last delimiter
			const std::string_view a = str.substr(offset, pos - offset);

			if (a != delimiter && a.size() > 0)
				result.push_back(std::stoi(std::string(a.begin(), a.end())));

			break;
		}
	}

	return result;
}

inline int charToInt(char c) {
	return int(c) - 48;
}

std::vector<int> splitDigits(std::string_view str)
{
	std::vector<int> result;

	for (auto c : str)
		result.push_back(charToInt(c));

	return result;
}

std::vector<std::string> splitStrings(std::string_view str, std::string_view delimiter = " ")
{
	std::size_t pos{ 0 };
	std::size_t offset{ 0 };
	std::vector<std::string> result;

	while (offset < str.length()) {

		pos = str.find(delimiter, offset);

		if (pos != std::string_view::npos) {

			const std::string_view a = str.substr(offset, pos - offset);

			if (a != delimiter && a.size() > 0)
				result.emplace_back(a.begin(), a.end());

			offset = pos + delimiter.length();
		}
		else {
			// check if we have one more value after the last delimiter
			const std::string_view a = str.substr(offset, pos - offset);

			if (a != delimiter && a.size() > 0)
				result.emplace_back(a.begin(), a.end());

			break;
		}
	}

	return result;
}