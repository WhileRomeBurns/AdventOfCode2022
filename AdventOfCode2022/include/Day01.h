#pragma once

#include <string>
#include <vector>
#include <span>
#include <algorithm>

#include "Utilities.h"

#define DAY day01

namespace DAY {
	const auto day = STRINGIFY(DAY);

	void compute(std::span<std::vector<int>> elfs) {
		auto mostCalories = 0;
		std::vector<int> totals;

		for (const auto& elf : elfs) {
			auto sum = 0;
			for (auto meal : elf)
				sum += meal;

			if (sum > mostCalories)
				mostCalories = sum;

			totals.push_back(sum);
		}

		auto s = totals.size();
		std::sort(totals.begin(), totals.end());
		auto topThree = totals[s - 1] + totals[s - 2] + totals[s - 3];

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tMost Calories: {}", mostCalories);
		fprintn("  Part 2");
		fprintn("\tTop Three    : {}", topThree);
	}

	void run() {
		auto data = readInputTxt(day);

		std::vector<std::vector<int>> elfs;
		std::vector<int> elf;

		for (const auto& line : data) {
			if (line.empty()) {
				elfs.push_back(elf);
				elf.clear();
			}
			else
				elf.push_back(std::stoi(line));
		}

		compute(elfs);
	}
} // namespace

#undef DAY