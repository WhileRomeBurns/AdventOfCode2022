#pragma once

#include <vector>
#include <span>
#include <string>
#include <algorithm>

#include "AdventTypes.h"
#include "Utilities.h"

#define DAY day14

namespace DAY {
	const auto day = STRINGIFY(DAY);

	using Grid = std::vector<std::vector<char>>;
	using GridSpan = std::span<std::vector<char>>;

	char getGrid(GridSpan grid, const Pos& p) { return grid[p.y][p.x]; }
	void setGrid(GridSpan grid, const Pos& p, char v) { grid[p.y][p.x] = v; }

	void printGrid(const auto& scan) {
		for (auto i = 0; const auto & row : scan) {
			fprint("{:2} ", i++);
			for (const auto& s : row)
				fprint("{}", s);
			fprint("\n");
		}
	}

	bool validPos(GridSpan scan, const Pos& pos) {
		if (pos.x < 0) return false;
		if (pos.y < 0) return false;
		if (pos.y >= scan.size()) return false;
		if (pos.x >= scan[0].size()) return false;
		return true;
	}

	bool isAir(GridSpan scan, const Pos& pos) {
		return getGrid(scan, pos) == '.';
	}

	int extendGrid(GridSpan scan, bool rhs) {
		auto width = int(scan[0].size());
		auto newWidth = int(float(width) * 1.5f);
		auto offset = newWidth - width;

		auto dir = rhs ? "right" : "left";
		// fprintn("Extending grid : {} by {} columns.", dir, offset);

		for (int r = 0; auto & row : scan) {
			if (rhs) {
				row.resize(newWidth);
				// fill new cells with air or floor
				for (int i = width; i < newWidth; i++)
					row[i] = r == scan.size() - 1 ? '#' : '.';
			}
			else {
				row.resize(newWidth);
				// in place copy from old range descending over to new range descending
				for (int i = width - 1; i >= 0; i--)
					row[i + offset] = row[i];
				// fill new cells with air or floor
				for (int i = 0; i < offset; i++)
					row[i] = r == scan.size() - 1 ? '#' : '.';
			}
			r++;
		}

		return offset;
	}

	int simulate(GridSpan scan, const Pos& minPos) {
		auto srcPos = Pos{ 500 - minPos.x, 0 - minPos.y };
		setGrid(scan, srcPos, '+');

		auto totalResting = 0;
		auto leaking = false;
		while (!leaking) {
			// introduce one grain
			auto simulatingGrain = true;
			auto currentPos = srcPos;

			while (simulatingGrain) {
				// apply rules to falling grain
				auto down = Pos{ currentPos.x, currentPos.y + 1 };
				auto left = Pos{ currentPos.x - 1, currentPos.y + 1 };
				auto right = Pos{ currentPos.x + 1, currentPos.y + 1 };

				auto downBlocked = false;
				auto leftBlocked = false;
				auto rightBlocked = false;

				// down
				if (validPos(scan, down)) {
					if (isAir(scan, down))
						currentPos = down;
					else
						downBlocked = true;
				}
				else {
					// we fell off downwards
					simulatingGrain = false;
					leaking = true;
					break;
				}

				// left and down
				if (downBlocked) {
					if (validPos(scan, left)) {
						if (isAir(scan, left))
							currentPos = left;
						else
							leftBlocked = true;
					}
					else {
						// we fell off the left downward side
						simulatingGrain = false;
						leaking = true;
						break;
					}
				}

				// right and down
				if (downBlocked && leftBlocked) {

					if (validPos(scan, right)) {
						if (isAir(scan, right))
							currentPos = right;
						else
							rightBlocked = true;
					}
					else {
						// we fell off the right downward side
						simulatingGrain = false;
						leaking = true;
						break;
					}
				}

				if (downBlocked && leftBlocked && rightBlocked) {
					setGrid(scan, currentPos, 'o');
					simulatingGrain = false;
					totalResting++;
				}
			} // while simulating grain
		} // while over all grains
		return totalResting;
	}

	int simulate2(GridSpan scan, const Pos& minPos) {
		auto srcPos = Pos{ 500 - minPos.x, 0 - minPos.y };
		setGrid(scan, srcPos, '+');

		auto totalResting = 0;
		auto hitTop = false;
		while (!hitTop) {
			// introduce one grain
			auto simulatingGrain = true;
			auto currentPos = srcPos;

			while (simulatingGrain) {
				// apply rules to falling grain
				auto down = Pos{ currentPos.x, currentPos.y + 1 };
				auto left = Pos{ currentPos.x - 1, currentPos.y + 1 };
				auto right = Pos{ currentPos.x + 1, currentPos.y + 1 };

				auto downBlocked = false;
				auto leftBlocked = false;
				auto rightBlocked = false;

				// down
				if (validPos(scan, down)) {
					if (isAir(scan, down))
						currentPos = down;
					else
						downBlocked = true;
				}

				// left and down
				if (downBlocked) {
					if (!validPos(scan, left)) {
						auto offset = extendGrid(scan, false);
						// offset currentPos and srcPos as the grid has more units on left
						currentPos.x += offset;
						srcPos.x += offset;
						down.x += offset;
						left.x += offset;
						right.x += offset;
					}

					if (isAir(scan, left))
						currentPos = left;
					else
						leftBlocked = true;
				}

				// right and down
				if (downBlocked && leftBlocked) {
					if (!validPos(scan, right)) {
						extendGrid(scan, true); // no need to update currentPos, sourcePos
					}

					if (isAir(scan, right))
						currentPos = right;
					else
						rightBlocked = true;
				}

				if (downBlocked && leftBlocked && rightBlocked) {
					setGrid(scan, currentPos, 'o');
					simulatingGrain = false;
					totalResting++;

					if (currentPos.x == srcPos.x && currentPos.y == srcPos.y)
						hitTop = true;
				}
			} // while simulating grain
		} // while over all grains

		return totalResting;
	}

	void compute(GridSpan scan, GridSpan scan2, const Pos& minPos) {

		auto totalResting = simulate(scan, minPos);
		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tTotal Resting: {}", totalResting);

		auto totalResting2 = simulate2(scan2, minPos);
		fprintn("  Part 2");
		fprintn("\tTotal Resting: {}", totalResting2);
	}

	std::vector<std::vector<Pos>> parseLines(std::span<std::string> data, Pos& outMinPos, Pos& outMaxPos) {
		std::vector<std::vector<Pos>> lines;

		for (const auto& l : data) {
			std::vector<Pos> line;

			for (const auto& coord : splitStrings(l, " -> ")) {

				auto [x, y] = splitPairInt(coord, ",");
				if (x < outMinPos.x)
					outMinPos.x = x;
				if (x > outMaxPos.x)
					outMaxPos.x = x;
				if (y < outMinPos.y)
					outMinPos.y = y;
				if (y > outMaxPos.y)
					outMaxPos.y = y;

				line.push_back(Pos{ x,y });
			}
			lines.push_back(line);
		}

		return lines;
	}

	Grid initGrid(const Pos& minPos, const Pos& maxPos) {
		Grid scan;

		Pos width{ (maxPos.x - minPos.x) + 1, (maxPos.y - minPos.y) + 1 };
		fprintn("Width: {},{}", width.x, width.y);

		scan.resize(width.y);
		for (auto& row : scan)
			for (auto i = 0; i < width.x; i++)
				row.push_back('.');

		return scan;
	}

	void extendToFloor(Grid& scan) {
		std::vector<char> row(scan[0].size());

		for (auto k = 0; k < 2; k++) {
			for (auto i = 0; i < row.size(); i++) {
				row[i] = k == 0 ? '.' : '#';
			}
			scan.push_back(row);
		}
	}

	void placeWalls(GridSpan scan, std::span<std::vector<Pos>> lines, const Pos& minPos) {
		for (const auto& line : lines) {
			for (auto i = 0; i < line.size() - 1; i++) {
				const auto& a = line[i];
				const auto& b = line[i + 1];

				auto minX = std::min(a.x, b.x);
				auto maxX = std::max(a.x, b.x);
				auto minY = std::min(a.y, b.y);
				auto maxY = std::max(a.y, b.y);

				for (auto y = 0; y < (maxY - minY) + 1; y++)
					for (auto x = 0; x < (maxX - minX) + 1; x++)
						setGrid(scan, Pos{ (minX + x) - minPos.x, (minY + y) - minPos.y }, '#');
			}
		}
	}

	void run() {
		Pos minPos{ 999999, 0 };
		Pos maxPos{ 500, 0 };

		auto data = readInputTxt(day);

		auto lines = parseLines(data, minPos, maxPos);
		Grid scan = initGrid(minPos, maxPos);
		placeWalls(scan, lines, minPos);

		// duplicate cave scan and add the bottom two rows for part2
		Grid scan2(scan.size());
		std::copy(scan.begin(), scan.end(), scan2.begin());
		extendToFloor(scan2);

		compute(scan, scan2, minPos);
	}
} // namespace

#undef DAY