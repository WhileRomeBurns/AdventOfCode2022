#pragma once

#include <string>
#include <vector>
#include <span>
#include <deque>

#include "Utilities.h"

#define DAY day05

namespace DAY {
	const auto day = STRINGIFY(DAY);

	std::string topCrates(const std::span<std::deque<char>> crates) {
		std::string answer;
		for (const auto& crate : crates)
			answer.push_back(crate.front());
		return answer;
	}

	void crane(std::span<std::deque<char>> crates, const auto moves, bool part1) {
		for (const auto& move : moves) {
			auto count = move[0];
			auto src = move[1] - 1;
			auto dst = move[2] - 1;

			if (part1) {
				for (auto i = 0; i < count; i++) {
					auto top = crates[src].front();
					crates[src].pop_front();
					crates[dst].push_front(top);
				}
			}
			else {
				for (auto i = count - 1; i >= 0; i--)
					crates[dst].push_front(crates[src][i]);
				crates[src].erase(crates[src].begin(), crates[src].begin() + count);
			}
		}
	}

	void compute1(std::vector<std::deque<char>> crates, const std::span<std::vector<int>> moves) {
		// not a span, we want a copy
		crane(crates, moves, true);

		fprintn("\n[{}]\n  Part 1", day);
		fprintn("\tTop Crates: {}", topCrates(crates));
	}

	void compute2(std::span<std::deque<char>> crates, const std::span<std::vector<int>> moves) {
		crane(crates, moves, false);

		fprintn("  Part 2");
		fprintn("\tTop Crates: {}", topCrates(crates));
	}

	void run() {
		auto data = readInputTxt(day);

		std::vector<std::deque<char>> crates(9);
		for (auto i = 0; i < 8; i++) {
			for (auto c = 0; c < 9; c++) {
				const auto crate = data[i].substr(1 + 4 * c, 1).c_str()[0];
				if (crate != ' ')
					crates[c].push_back(crate);
			}
		}

		std::vector<std::vector<int>> moves;
		for (auto i = 10; i < data.size(); i++) {
			const auto word = splitStrings(data[i]);
			moves.emplace_back(std::vector{ strTo<int>(word[1]), strTo<int>(word[3]), strTo<int>(word[5]) });
		}

		compute1(crates, moves);
		compute2(crates, moves);
	}
} // namespace

#undef DAY