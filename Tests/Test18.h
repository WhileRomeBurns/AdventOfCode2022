#pragma once

//#include "../AdventOfCode2021/include/Day18b.h"
//
//namespace day18 {
//
//	std::string roundTrip(std::string snailStr) {
//		auto snail = parseSnail(snailStr);
//		resolveParents(snail);
//
//		return snailToString(snail.get());
//	}
//
//	std::string roundTripAdd(std::string snailStrLhs, std::string snailStrRhs) {
//		auto lhs = parseSnail(snailStrLhs);
//		resolveParents(lhs);
//
//		auto rhs = parseSnail(snailStrRhs);
//		resolveParents(rhs);
//
//		auto snail = addSnail(lhs, rhs);
//
//		return snailToString(snail.get());
//	}
//
//	std::string decendTest(std::string snailStr, bool left = true) {
//		auto snail = parseSnail(snailStr);
//		resolveParents(snail);
//
//		auto snailChild = descend(snail, left);
//		return snailToString(snailChild.get());
//	}
//
//	std::string accendFromBottom(std::string snailStr, bool left = true) {
//		auto snail = parseSnail(snailStr);
//		resolveParents(snail);
//
//		auto snailChild = descend(snail, left);
//		auto snailTopParent = ascend(snailChild);
//		return snailToString(snailTopParent.get());
//	}
//
//	std::pair<std::string, std::string> flatlistPair(std::string snailStr) {
//		auto snail = parseSnail(snailStr);
//		resolveParents(snail);
//
//		std::vector<std::pair<WeakSnailPtr, Side>> flatList;
//		buildFlatList(snail, flatList);
//
//		return flatListToString(flatList);
//	}
//
//	//std::string addToAdjacentTest(std::string snailStr, int targetSnailNumber) {
//	//	auto snail = parseSnail(snailStr);
//	//	resolveParents(snail);
//	//
//	//	//return snailToString(snail.get());
//	//}
//
//}