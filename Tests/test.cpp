#include "pch.h"

//#include "Test18.h"

//namespace day18 {
//	TEST(Day18b, IsRegular) {
//		EXPECT_EQ(isRegular("[1,1]"), false);
//		EXPECT_EQ(isRegular("1,1"), true);
//		EXPECT_EQ(isRegular("[1,1"), false);
//		EXPECT_EQ(isRegular("1,1]"), false);
//		EXPECT_EQ(isRegular("[1,[2,5]]"), false);
//		EXPECT_EQ(isRegular("[[9,9],[2,5]]"), false);
//	}
//
//	TEST(Day18b, OuterComma) {
//		EXPECT_EQ(outerComma("[1,1]"), 2);
//		EXPECT_EQ(outerComma("[1,[3,3]]"), 2);
//		EXPECT_EQ(outerComma("[[8,2],4]"), 6);
//		EXPECT_EQ(outerComma("[[[2,4],5],[7,[1,9]]]"), 10);
//		EXPECT_EQ(outerComma("[[[2,4],[7,5]],[7,[1,9]]]"), 14);
//	}
//
//	TEST(Day18b, SnailConstruction) {
//		auto s1 = std::string("[1,2]");
//		EXPECT_EQ(s1, roundTrip(s1));
//		auto s2 = std::string("[[1,2],3]");
//		EXPECT_EQ(s2, roundTrip(s2));
//		auto s3 = std::string("[9,[8,7]]");
//		EXPECT_EQ(s3, roundTrip(s3));
//		auto s4 = std::string("[[1,9],[8,5]]");
//		EXPECT_EQ(s4, roundTrip(s4));
//		auto s5 = std::string("[[[[1,2],[3,4]],[[5,6],[7,8]]],9]");
//		EXPECT_EQ(s5, roundTrip(s5));
//		auto s6 = std::string("[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]");
//		EXPECT_EQ(s6, roundTrip(s6));
//		auto s7 = std::string("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]");
//		EXPECT_EQ(s7, roundTrip(s7));
//		auto s8 = std::string("[[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]],[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]]");
//		EXPECT_EQ(s8, roundTrip(s8));
//	}
//
//	TEST(Day18b, Descend) {
//		auto s1 = std::string("[[1,2],3]");
//		EXPECT_EQ(std::string("[1,2]"), decendTest(s1, true));
//		EXPECT_EQ(std::string("[[1,2],3]"), decendTest(s1, false));
//
//		auto s2 = std::string("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]");
//		EXPECT_EQ(std::string("[1,3]"), decendTest(s2, true));
//		EXPECT_EQ(std::string("[7,3]"), decendTest(s2, false));
//	}
//
//	TEST(Day18b, Ascend) {
//		auto s1 = std::string("[[1,2],3]");
//		EXPECT_EQ(s1, accendFromBottom(s1, true));
//		EXPECT_EQ(s1, accendFromBottom(s1, false));
//
//		auto s2 = std::string("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]");
//		EXPECT_EQ(s2, accendFromBottom(s2, true));
//		EXPECT_EQ(s2, accendFromBottom(s2, false));
//	}
//
//	TEST(Day18b, Addition) {
//		EXPECT_EQ(std::string("[[1,4],[2,2]]"), roundTripAdd("[1,4]", "[2,2]"));
//		EXPECT_EQ(std::string("[[[1,4],7],[2,[4,4]]]"), roundTripAdd("[[1,4],7]", "[2,[4,4]]"));
//	}
//
//	TEST(Day18b, Flatlist) {
//		auto s1 = std::string("[[1,2],3]");
//		auto s1p = flatlistPair(s1);
//		EXPECT_EQ("0 1 2", s1p.first);
//		EXPECT_EQ("1 2 3", s1p.second);
//
//		//std::cout << s1p.first << std::endl;
//		//std::cout << s1p.second << std::endl;
//
//		auto s2 = std::string("[[1,4],[2,2]]");
//		auto s2p = flatlistPair(s2);
//		EXPECT_EQ("0 1 2 3", s2p.first);
//		EXPECT_EQ("1 4 2 2", s2p.second);
//
//		//std::cout << s2p.first << std::endl;
//		//std::cout << s2p.second << std::endl;
//
//		auto s3 = std::string("[[[[1,2],[3,4]],[[5,6],[7,8]]],9]");
//		auto s3p = flatlistPair(s3);
//		EXPECT_EQ("0 1 2 3 4 5 6 7 8", s3p.first);
//		EXPECT_EQ("1 2 3 4 5 6 7 8 9", s3p.second);
//
//		auto s4 = std::string("[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]");
//		auto s4p = flatlistPair(s4);
//		EXPECT_EQ("0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15", s4p.first);
//		EXPECT_EQ("1 3 5 3 1 3 8 7 4 9 6 9 8 2 7 3", s4p.second);
//	}
//}